namespace PacmanIOS

open System
open System.Drawing
open System.Collections.Generic
open System.Runtime.InteropServices

open Microsoft.FSharp.NativeInterop

open MonoTouch.Foundation
open MonoTouch.UIKit
open MonoTouch.CoreGraphics
open MonoTouch.CoreImage
open MonoTouch.ObjCRuntime
open MonoTouch.CoreAnimation

#nowarn "9"

open PacMan

[<AutoOpen>]
module Helper = 
    let screenWidth = UIScreen.MainScreen.Bounds.Width
    let scale = screenWidth / 240.0f
    
    let swizzle = function
    | [|b;g;r;a|] -> [|r;g;b;a|]
    | _ -> failwith "never"
        
    let debug s = System.Diagnostics.Debug.WriteLine s
    
    let view (content:IContent) = content.Control:?>UIView

type Content(view:UIView) =
    do view.Hidden <- true
    interface IContent with
        member __.Control = view :> obj
        member __.Move(x,y) = 
            let mutable frame = view.Frame
            frame.X <- float32 x * scale
            frame.Y <- float32 y * scale
            view.Frame<-frame
        member __.SetOpacity f = 
            view.Alpha <- float32 f
       
type Contents(parent:UIView) = 
     let hash = HashSet()
     let all = HashSet()
     interface IContents with
        member __.Add c = 
            if not (all.Contains c) then
                ignore <| all.Add c
                parent.Add (view c)
            hash.Add c |> ignore
            (view c).Hidden <- false
        member __.Remove c = 
            hash.Remove c|> ignore
            let control = (view c)
            control.Hidden <- true
        member __.Contains c = hash.Contains c
     
type TextContent(parent) as textContent = 
    inherit Content(new UITextView(RectangleF(0.f,0.f,400.f,25.f), TextColor=UIColor.White, BackgroundColor=new UIColor(0.f,0.f,0.f,0.f)))
    let content = textContent :> IContent
    let textView = content.Control :?> UITextView
    do textView.Font <- UIFont.FromName("Arial", 10.f)
    interface ITextContent with
        member __.SetText text = 
            textView.Text <- text
            
type BitmapContent(width:int, height:int, data:int[], offset:float) = 
    let byteArray = data|>Array.collect( fun i ->  BitConverter.GetBytes(i) |> swizzle)
    let data = NSData.FromArray(byteArray)
    let buffer = Marshal.AllocHGlobal(int data.Length)
    let colorSpace = CGColorSpace.CreateDeviceRGB()
    let context = new CGBitmapContext(buffer, width, height, 8, 4*width,colorSpace, CGImageAlphaInfo.PremultipliedLast)
    do colorSpace.Dispose()
    do context.InterpolationQuality <- CGInterpolationQuality.High
    do context.SetAllowsAntialiasing true
    let blit() = 
        let bufptr = NativePtr.ofNativeInt buffer
        for i in 0..(int byteArray.Length-1) do
            NativePtr.set bufptr i byteArray.[i]
    do blit()
    let image = UIImage.FromImage (context.ToImage())
    do context.Dispose()
    do Marshal.FreeHGlobal buffer
    interface IBitmap with 
        member __.CreateContent() = 
            let image' = new UIImageView(new RectangleF(0.0f,0.0f,float32 width*scale,float32 height*scale))
            do image'.Image <- image
            Content(image') :> IContent
            
type Layer() as this = 
    inherit Content(new UIControl())
    let content = this :> IContent
    let parent = view content
    let contents = Contents(parent)
    interface ILayer with
        member __.Contents = contents :> IContents
      
type Scene(parent:UIView) = 
    let createBitmap (width, height, offset) (data:int[][]) =
        let data' = ResizeArray()
        for x in 0..data.Length-1 do
            for y in 0..data.[x].Length-1 do
                data'.Add data.[x].[y]
        BitmapContent(width, height, data'|>Seq.toArray, offset) :> IBitmap
    let contents = Contents(parent) :> IContents 
    interface IScene with
        member x.AddLayer() = 
            let layer = Layer() :> ILayer
            let content = layer :> IContent
            contents.Add content
            layer
        member __.CreateBitmap (paint, lines) = 
            let lines = lines |> Seq.toArray
            let width, height = 8, lines.Length
            let white = paint.Color
            let black = 0x00000000
            let toColor = function true -> white | false -> black
            lines |> Array.mapi (fun y line ->
                Array.init width (fun x ->
                    let bit = 1 <<< (width - 1 - x) 
                    line &&& bit = bit |> toColor
                )
            )
            |> createBitmap (width,height, 0.)
        member __.CreateBitmap (w,h,data) =
            createBitmap (w,h,3.5) data
        member __.CreateText s = 
            let textContent = TextContent(parent) :> ITextContent
            textContent.SetText s
            textContent
        member x.Contents = contents
   
[<Register ("PacmanIOSViewController")>]
type PacmanIOSViewController() as this =
    inherit UIViewController ()
    
    let mutable direction = None
    
    let addSwipeGesture d (view:UIView) = 
        let gesture = new UISwipeGestureRecognizer()
        ignore <| gesture.AddTarget(fun () -> direction <- Some(d))
        gesture.Direction <- d
        view.AddGestureRecognizer gesture
        
    let isDirection d =
        match direction with
        | Some(direction) when d = direction -> true
        | _ -> false
        
    override x.ViewDidLoad () =
        base.ViewDidLoad ()
        addSwipeGesture UISwipeGestureRecognizerDirection.Right x.View
        addSwipeGesture UISwipeGestureRecognizerDirection.Left x.View
        addSwipeGesture UISwipeGestureRecognizerDirection.Up x.View
        addSwipeGesture UISwipeGestureRecognizerDirection.Down x.View
        let pacmanGame = Game(Scene(this.View) :> IScene, 
                            { new IInput with
                                member __.IsUp = isDirection UISwipeGestureRecognizerDirection.Up
                                member __.IsDown = isDirection UISwipeGestureRecognizerDirection.Down
                                member __.IsRight = isDirection UISwipeGestureRecognizerDirection.Right
                                member __.IsLeft = isDirection UISwipeGestureRecognizerDirection.Left })
        
        let displayLink = CADisplayLink.Create(fun () -> pacmanGame.Update())   
        displayLink.AddToRunLoop(NSRunLoop.Current, NSRunLoop.NSDefaultRunLoopMode)  
        ()                    


    override x.ShouldAutorotateToInterfaceOrientation (toInterfaceOrientation) =
        toInterfaceOrientation = UIInterfaceOrientation.Portrait
    